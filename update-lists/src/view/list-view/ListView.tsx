import {UserStore} from "../../some-store/UserStore";
import React, {useEffect, useState} from "react";
import {UserView} from "./UserView";

interface Props {
    store: UserStore
}

const useList = (store: UserStore) => {
    const [state, setState] = useState(store.getList());
    useEffect(() => {
        return store.listChanged.onChange((data) => {
            setState(data);
        })
    })
    return [state];
}

let repaints = 0;

export const ListView = (props: Props) => {
    const [list] = useList(props.store)
    return <><h1>{repaints++}</h1>
        <div className="CrazyUserViewContainer">
            {list.map(u => <UserView key={u} id={u} store={props.store}/>)}
        </div>
    </>
}
