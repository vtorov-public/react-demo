import {UserStore} from "../../some-store/UserStore";
import React, {useEffect, useState} from "react";
import {UserView as User} from '../user-view/UserView';

interface Props {
    store: UserStore
    id: string
}

const useUser = (store: UserStore, id: string) => {
    const [state, setState] = useState(store.getUser(id));
    useEffect(() => {
        return store.userWithIdChanged.onChange(id, (data) => {
            console.log('change detected', {id})
            setState({...data});
        })
    })
    return [state];
}

let repaints = 0;

export const UserView = (props: Props) => {
    const [user] = useUser(props.store, props.id)
    if (!user) {
        return <div className="CrazyListItem"><h3>{repaints++}</h3>
            <div className="CrazyUserView">
                NOPE {props.id}
            </div>
        </div>
    }
    return <div className="CrazyListItem">
        <h3>{repaints++}</h3>
        <div className="CrazyUserView">
            <User user={user}/>
        </div>
    </div>
}
