import {UserStore} from "../../some-store/UserStore";
import React, {useEffect, useState} from "react";
import {UserView} from "../user-view/UserView";

interface Props {
    store: UserStore
}

const useList = (store: UserStore) => {
    const [state, setState] = useState(store.getUserList());
    useEffect(() => {
        return store.userListChanged.onChange((data) => {
            setState([...data]);
        })
    })
    return [state];
}

let repaints = 0;

export const UserListView = (props: Props) => {
    const [list] = useList(props.store)
    return <><h1>{repaints++}</h1>
        <div className="UserListViewContainer">
            {list.map(u => <UserView key={u.id} user={u}/>)}
        </div>
    </>
}
