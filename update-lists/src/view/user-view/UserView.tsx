import {User} from "../../some-store/UserStore";

interface Props {
    user: User
}

let repaints = 0;

export const UserView = (props: Props) => {
    return <div className="User">
        <div className="User__repaints">{repaints++}</div>
        <div className="User__id">{props.user.id}</div>
        <div className="User__description">{props.user.description}</div>
    </div>
}
