import {EventEmitter, FilterEventEmitter} from "../event-emitter/EventEmitter";
import {removeRandomArrayElement, rnd, rndBoolean, rndString, Timeout} from "../util";


export interface User {
    id: string;
    description: string;
}

export type List = string[];
export type UserList = User[];

interface Modify {
    changed: boolean;
    userChanged: Set<string>;
}

export class UserStore {
    constructor(readonly size = 1000) {
    }

    private interval: Timeout | null = null;

    start() {
        if (this.interval) {
            return;
        }
        this.interval = setInterval(this.change.bind(this), 100);
    }

    stop() {
        if (!this.interval) {
            return;
        }
        clearInterval(this.interval);
        this.interval = null;
    }

    change() {
        const modify: Modify = {
            changed: false,
            userChanged: new Set<string>()
        }
        if (this.users.length < this.size / 2) {
            this.fillTo(this.size, modify);
        }
        if (this.users.length > this.size * 2) {
            this.killTo(this.size, modify);
        }
        if (rndBoolean(0.02)) {
            this.add(1, modify);
        }
        if (rndBoolean(0.02)) {
            this.kill(1, modify);
        }
        if (rndBoolean(0.001)) {
            this.add(rnd(this.size / 10), modify);
        }
        if (rndBoolean(0.001)) {
            this.kill(rnd(this.size / 10), modify);
        }
        if (rndBoolean(0.2)) {
            this.changeUsers(modify);
        }
        this.emit(modify);
    }

    users: User[] = [];

    getList(): List {
        return this.users.map(u => u.id);
    }

    getUserList(): UserList {
        return this.users;
    }

    getUser(id: string): User | undefined {
        return this.users.find(u => u.id === id);
    }

    private readonly listChangedEmitter = new EventEmitter<List>('listChanged');
    private readonly userListChangedEmitter = new EventEmitter<UserList>('userListChanged');
    private readonly userChangedEmitter = new EventEmitter<User>('userChanged');

    public readonly listChanged = this.listChangedEmitter.readonly();
    public readonly userListChanged = this.userListChangedEmitter.readonly();
    public readonly userChanged = this.userChangedEmitter.readonly();
    public readonly userWithIdChanged = new FilterEventEmitter(this.userChanged, (user) => user.id);

    private fillTo(size: number, modify: Modify) {
        console.log('fillTo');
        this.add(size - this.users.length, modify);
    }

    private killTo(size: number, modify: Modify) {
        console.log('killTo');
        this.kill(this.users.length - size, modify);
    }

    private add(number: number, modify: Modify) {
        if (number < 1) {
            return;
        }
        console.log('add');
        for (let i = 0; i < number; i++) {
            this.users.push(this.newUser());
        }
        modify.changed = true;
    }

    private kill(number: number, modify: Modify) {
        if (number < 1) {
            return;
        }
        console.log('kill');
        for (let i = 0; i < number; i++) {
            this.users = removeRandomArrayElement(this.users);
        }
        modify.changed = true;
    }

    private changeUsers(modify: Modify) {
        this.users.forEach(u => {
            if (rndBoolean(0.075)) {
                u.description = new Date().toISOString().split('T')[1] + ' ' + rndString();
                console.log('changeUser', {id: u.id, description: u.description});
                modify.userChanged.add(u.id);
            }
        })
    }

    lastId = 0;

    private newUser(): User {
        return {
            id: '#' + (this.lastId++),
            description: 'default ' + rndString()
        }
    }

    private emit(modify: Modify) {
        Array.from(modify.userChanged.keys())
            .forEach(
                u => {
                    const user = this.getUser(u);
                    if (user) {
                        this.userChangedEmitter.emit(user)
                    }
                }
            );
        if (modify.changed) {
            this.listChangedEmitter.emit(this.getList());
        }
        if (modify.changed || modify.userChanged.size > 0) {
            this.userListChangedEmitter.emit(this.getUserList());
        }
    }
}
