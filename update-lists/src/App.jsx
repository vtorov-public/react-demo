import './App.css';
import {UserStore} from "./some-store/UserStore";
import React from "react";
import {ListView} from "./view/list-view/ListView";
import {UserListView} from "./view/user-list-view/UserListView";

const w = window;
if (!w.store) {
    w.store = new UserStore(30);
}
const store = w.store;
store.start();

function App() {
    return (
        <div className="App">
            <div className="CrazyUserView UserView">
                <ListView store={store}/>
            </div>
            <div className="UserListView UserView">
                <UserListView store={store}/>
            </div>
        </div>
    );
}

export default App;
