export type changeListener<T> = (data: T) => void;
export type unsubscribeDelegate = () => void;

export interface ReadonlyEventEmitter<T> {
    onChange(listener: changeListener<T>): unsubscribeDelegate;
}

interface EventEmitterSubscription<T> {
    callback: changeListener<T>;
}

export class FilterEventEmitter<T, K> {
    constructor(readonly emitter: ReadonlyEventEmitter<T>, readonly mapper: (data: T) => K) {
    }

    onChange(filter: K, listener: changeListener<T>): unsubscribeDelegate {
        const coll = this.listeners.get(filter);
        const sub = {callback: listener};
        if (coll) {
            coll.push(sub);
        } else {
            this.listeners.set(filter, [sub])
        }
        if (this.listeners.size) {
            this.subscribe();
        } else {
            this.unsubscribe();
        }
        return () => {
            const coll = this.listeners.get(filter);
            if (!coll) {
                return;
            }
            const newColl = coll.filter(s => s !== sub);
            if (newColl.length) {
                this.listeners.set(filter, newColl);
            } else {
                this.listeners.delete(filter);
                if (!this.listeners.size) {
                    this.unsubscribe();
                }
            }
        }
    }

    unsub: unsubscribeDelegate | undefined;
    listeners = new Map<K, EventEmitterSubscription<T>[]>();

    private subscribe() {
        if (!this.unsub) {
            this.unsub = this.emitter.onChange((data) => this.change(data));
        }
    }

    private unsubscribe() {
        if (this.unsub) {
            this.unsub();
            this.unsub = undefined;
        }
    }

    private change(data: T) {
        if (this.listeners.size < 1) {
            return;
        }
        const key = this.mapper(data);
        const coll = this.listeners.get(key);
        if (coll) {
            coll.forEach(s => s.callback(data));
        }
    }
}

export class EventEmitter<T> implements ReadonlyEventEmitter<T> {
    constructor(name: string) {
    }

    subs: EventEmitterSubscription<T>[] = [];

    onChange(listener: changeListener<T>): unsubscribeDelegate {
        const sub = {callback: listener};
        this.subs.push(sub);
        return () => {
            this.subs = this.subs.filter(s => s !== sub);
        }
    }

    emit(data: T) {
        this.subs.forEach(s => s.callback(data));
    }

    readonly(): ReadonlyEventEmitter<T> {
        return {
            onChange: this.onChange.bind(this)
        }
    }
}
