export const rnd = (max: number) => {
    return Math.floor(Math.random() * max);
}

export const rndBoolean = (probability = 0.5) => {
    return Math.random() < probability;
}

const d = '1234567890'
const a = 'qwertyuiopasdfghjklzxcvbnm';
const ab = d + a + a.toUpperCase();

export const rndString = (length = 6) => {
    let ret = '';
    while (ret.length < length) {
        ret += ab.charAt(rnd(ab.length));
    }
    return ret;
}

export const removeRandomArrayElement = <T>(array: T[]): T[] => {
    const at = rnd(array.length);
    const ret = [...array.slice(0, at), ...array.slice(at + 1)];
    console.log({removeRandomArrayElement: ret, array, at});
    return ret;
}

export type Timeout = ReturnType<typeof setTimeout>;
